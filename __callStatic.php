<?php
class Car{
    public $brand;
    public $price;
    public $speed;

    public static function __callStatic($name, $arguments)
    {
        echo "I am inside ".__METHOD__."<br>";
        echo "Wrong Method Name: $name<br>";

        echo "<pre>";
        var_dump($arguments);
        echo "</pre>";
    }
    public static function just_a_function(){

    }
}

Car::just_b_function("AB Akas");
