<?php
class Car{
    public $brand;
    public $price;
    public $speed;

    public function __call($name, $arguments)
    {
        echo "I am inside ".__METHOD__."<br>";
        echo "Wrong Method Name: $name<br>";

        echo "<pre>";
            var_dump($arguments);
        echo "</pre>";
    }
    public function just_a_function(){

    }
}
$object = new Car();

$object->just_b_function("AB Akas");