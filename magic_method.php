<?php
class person{

    public $name = "Default Name.";
    public $address = "Default Address";
    public $phoneNumber = "Default Phone Number.";

    public static $myStatic;

    public function __construct()
    {
        echo "I am inside".__METHOD__."<br>";
    }
    public function __call($name, $arguments)
    {
        echo "I am inside".__METHOD__."<br>";
        echo "Wrong Method Name = $name";

        echo "<pre>";
            print_r($arguments);
        echo "</pre>";
    }
    public static function __callStatic($name, $arguments)
    {
        echo "I am inside".__METHOD__."<br>";
        echo "Wrong Static Method  Name = $name";

        echo "<pre>";
        print_r($arguments);
        echo "</pre>";
    }
    public function doSomething(){

    }

    public static function doFrequently(){
        echo "I am Doing it Frequently<br>";
    }
    public function __set($name, $value)
    {
        echo "I am inside".__METHOD__."<br>";
        echo "Wrong Property Name = $name<br>";
        echo "Value tried to set to that Wrong Property is = $value";
    }

    public function __get($name)
    {
        echo "I am inside".__METHOD__."<br>";
        echo "Wrong Property Method  Name = $name<br>";
    }
    public function __isset($name)
    {
        echo "I am inside".__METHOD__."<br>";
        echo "Wrong Property Method  Name = $name<br>";    }
    public function __unset($name)
    {
        echo "I am inside".__METHOD__."<br>";
        echo "Wrong Property Method  Name = $name<br>";    }
    public function __sleep()
    {
        return array("name","address");
    }
    public function __wakeup()
    {
        $this->doSomething();
    }
    public function __toString()
    {
        return "Are You crazy! I am Just a Object, Not a String.";
    }
    public function __invoke($value)
    {
        echo "I am in side ".__METHOD__."<br>";
        echo "Value = ".$value."<br>";
    }
}
/*class Dummy{
    public $dummy;
    public function __construct()
    {
        echo "123";
    }
    public function __destruct()
    {
        echo "789";
    }
}*/

$obj = new person();
echo "Hello World!<br>";

$obj->doSomethin();

person::$myStatic = "Hello Static";
person::doFrequently();
echo person::$myStatic;

person::doFrequentl();

$obj->dob = "Set This String";
echo $obj->dobhkjd;

if(isset($obj->dob)){}

unset($obj->dob);


$myVar =  serialize($obj);

var_dump($myVar);
echo "<br>";
$newObject = unserialize($myVar);

var_dump($newObject);

echo "<br>".$obj."<br>";

$obj(56);

unset($obj);