<?php
class Car{
    public $brand;
    public $price;
    public $speed;

    public function __destruct()
    {
        echo "I am inside ".__METHOD__."<br>";
    }
}

$obj = new Car();

unset($obj); //Destroy The Object Here

?>